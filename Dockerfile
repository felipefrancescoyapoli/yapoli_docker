FROM php:7.4-cli-alpine

## Install SSH and Rsync
RUN apk add --no-cache unzip libpng-dev openssh-client git curl

## Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

RUN docker-php-ext-install gd

## Add Composer vendor into PATH
ENV PATH /root/.composer/vendor/bin:$PATH
